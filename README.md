# plaiforlive

Connect your Liveset to the plaitools framework.

Send JSON formatted TCP messages to control your liveset.

Options:

* **play** - fire clips or scenes
* **stop** - stop clips or tracks
* **mix** - change mixer device values
* **set** - set properties of clips, tracks or scenes (not accessible yet)

check out the Live Object Model:  
https://docs.cycling74.com/max5/refpages/m4l-ref/m4l_live_object_model.html#MixerDevice

## JSON Schema

```json
"play" : {
            "type" : "object",
            "properties" : {
                "clip" : {
                    "type" : "string"
                },
                "scene" : {
                    "type" : "string"
                }
            }
        },
        "stop" : {
            "type" : "object",
            "properties" : {
                "track" : {
                    "type" : "string"
                },
                "clip" : {
                    "type" : "string"
                }
            }
        },
        "mix" : {
            "type" : "object",
            "properties" : {
                "track" : {
                    "type" : "string",
                    "required" : true
                },
                "send" : {
                    "required" : true,
                    "type" : "string",
                    "enum" : [ 
                        "A", 
                        "B", 
                        "C", 
                        "D", 
                        "E", 
                        "F", 
                        "G", 
                        "H", 
                        "I", 
                        "J", 
                        "K", 
                        "L", 
                        "M", 
                        "N", 
                        "O", 
                        "P", 
                        "Q", 
                        "R", 
                        "S", 
                        "T", 
                        "U", 
                        "V", 
                        "W", 
                        "X", 
                        "Y", 
                        "Z"
                    ]
                },
                "value" : {
                    "required" : true,
                    "type" : "number",
                    "format" : "range",
                    "minimum" : 0,
                    "maximum" : 1,
                    "step" : "0.01"
                }
            }
        }
```
